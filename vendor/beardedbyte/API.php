<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 12/07/2018
 * Time: 14:17
 */

namespace BeardedByte;


class API extends Controller {

    public function __construct(Application $application) {
        parent::__construct($application);
    }

    public function responseStatement($type, $code, $messageText, $messageHTML, $payload) {
        return array(
            'type' => $type,
            'code' => $code,
            'messageText' => $messageText,
            'messageHTML' => $messageHTML,
            'payload' => $payload
        );
    }

    public function successStatement($payload) {
        return $this->responseStatement('success', 200, '', '', $payload);
    }

    public function errorStatement($code, $messageText, $messageHTML=null) {
        if ($messageHTML === null) {
            $messageHTML = '<p>'.$messageText.'</p>';
        }
        return $this->responseStatement('error', $code, $messageText, $messageHTML, null);
    }

    public function notFoundStatement($message="La ressource demandé n'existe pas") {
        return $this->errorStatement(404, $message, '<p>'.$message.'</p>');
    }

    public function unauthorisedStatement($message="Vous n'avez pas le droit d'accéder à cette ressource") {
        return $this->errorStatement(403, $message, '<p>'.$message.'</p>');
    }
}