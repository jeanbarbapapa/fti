<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 12/07/2018
 * Time: 14:13
 */

namespace BeardedByte;

use Klein\Klein;

class Controller {

    /**
     * @var Application
     */
    public $application;

    /**
     * @var Klein
     */
    public $router;

    /**
     * @var \Twig_Environment
     */
    public $twig;

    /**
     * @var Container
     */
    public $container;

    /**
     * Controller constructor.
     *
     * @param Application $application
     */
    public function __construct(Application $application) {
        $this->application = $application;
        $this->router = $application->router;
        $this->twig = $application->twig;
        $this->container = $application->container;
        $this->_register_routes();
    }

    /**
     * @param $template
     * @param $params
     */
    protected function render($template, $params=[]) {
        $this->application->render($template, $params);
    }

    /**
     *
     */
    protected function _register_routes() {

    }
}