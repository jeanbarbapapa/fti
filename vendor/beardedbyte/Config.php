<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 20/07/2018
 * Time: 21:17
 */

namespace BeardedByte;


class Config {

    /**
     * @var array
     */
    public $config;

    /**
     * Config constructor.
     *
     * @param string $filepath
     */
    public function __construct($filepath=__DIR__.'/../../config') {
        if (!file_exists($filepath)) {
            touch($filepath);
        }

        $config_content = file_get_contents($filepath);
        $config_lines = explode('\n', $config_content);

        foreach ($config_lines as $line) {
            $line_part = explode('=', $line, 2);
            $name = $line_part[0];
            $value = (count($line_part) == 2) ? $line_part[1] : null;
            $this->config[$name] = $value;
        }
    }

    /**
     * @param string $name
     * @param mixed $default
     *
     * @return null
     */
    public function get($name, $default=null) {
        if (array_key_exists($name, $this->config)) {
            return $this->config[$name];
        }
        return $default;
    }

}