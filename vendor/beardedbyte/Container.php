<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 20/07/2018
 * Time: 20:56
 */

namespace BeardedByte;


use Klein\Klein;


class Container {

    /**
     * @var Application
     */
    public $application;

    /**
     * @var array
     */
    public $databases;

    /**
     * @var array
     */
    public $modelsmanagers;

    /**
     * @var Klein
     */
    public $router;

    /**
     * @var \Twig_Environment
     */
    public $twig;

    /**
     * Container constructor.
     *
     * @param Application $application
     */
    public function __construct(Application $application) {
        $this->application = $application;
        $this->databases = [];
        $this->modelsmanagers = [];
        $this->router = new Klein();
        $this->config = new Config();
        $this->twig = new \Twig_Environment(new \Twig_Loader_Filesystem(__DIR__.'/../../templates'));
        $this->twig->addGlobal('session', $_SESSION);
    }

    /**
     * @param string $name
     * @param \PDO $database
     *
     * @return Container
     * @throws \Exception
     */
    public function add_database($name, \PDO $database) {
        if (array_key_exists($name, $this->databases)) {
            throw new \Exception('A database is already registred as "'.$name.'"');
        }
        $this->databases[$name] = $database;
        return $this;
    }

    /**
     * @param string $name
     *
     * @return \PDO
     * @throws \Exception
     */
    public function get_database($name) {
        if (array_key_exists($name, $this->databases)) {
            return $this->databases[$name];
        }
        throw new \Exception('No database is named "'.$name.'"');
    }

    /**
     * @param string $name
     * @param ModelManager $modelmanager
     *
     * @return $this
     * @throws \Exception
     */
    public function add_modelmanager($name, ModelManager $modelmanager) {
        if (array_key_exists($name, $this->modelsmanagers)) {
            throw new \Exception('A model manager is already registred as "'.$name.'"');
        }
        $this->modelsmanagers[$name] = $modelmanager;
        return $this;
    }

    /**
     * @param string $name
     *
     * @return ModelManager
     * @throws \Exception
     */
    public function get_modelmanager($name) {
        if (array_key_exists($name, $this->modelsmanagers)) {
            return $this->modelsmanagers[$name];
        }
        throw new \Exception('No model manager is named "'.$name.'"');
    }
}