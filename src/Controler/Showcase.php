<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 02/11/2018
 * Time: 18:43
 */

namespace FTI\Controler;


use BeardedByte\Application;
use BeardedByte\Controller;
use Klein\Exceptions\HttpException;
use Klein\Request;
use Klein\Response;

class Showcase extends Controller {

    /**
     * @var \FTI\Application
     */
    public $application;

    protected function _register_routes() {
        $this->router->respond('GET', '/?', [$this, 'index']);
        $this->router->respond('GET', '/dessins', [$this, 'dessins']);
        $this->router->respond('GET', '/dessins/[i:id]/image', [$this, 'dessin_image']);
        $this->router->respond('GET', '/archives', [$this, 'archives']);
        $this->router->respond('GET', '/couverture/[i:annee]-[i:mois]/[i:id]', [$this, 'couverture']);
        $this->router->respond('GET', '/lire/[i:annee]-[i:mois]/[i:id]', [$this, 'edition_pdf']);
    }

    public function index(Request $req, Response $rep) {
        $edition = $this->container->get_modelmanager('edition')->get_last();
        $this->render('showcase/index.html.twig', array('edition' => $edition));
    }

    public function dessins(Request $req, Response $rep) {
        $dessins = $this->container->get_modelmanager('dessin')->ordered_by_date();
        $this->render('showcase/dessins.html.twig', array('dessins' => $dessins));
    }

    public function dessin_image(Request $req, Response $rep) {
        $dessin = $this->container->get_modelmanager('dessin')->get($req->param('id'));
        if ($dessin & $dessin['publique']) {
            $filePath = $dessin['filepath'];
            header('Content-Type: '.mime_content_type($filePath));
            header('Content-Length: '.filesize($filePath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            readfile($filePath);
            throw HttpException::createFromCode(200);
        }
        throw HttpException::createFromCode(404);
    }

    public function archives(Request $req, Response $rep) {
        $_editions = $this->container->get_modelmanager('edition')->ordered_by_date();

        $editions = [];
        foreach ($_editions as $i => $edition) {


            $key = "";

            if ($edition['mois'] >= 8) {
                $key = $edition['annee'].' - '.($edition['annee']+1);
            } else {
                $key = ($edition['annee']-1).' - '.$edition['annee'];
            }

            if (!array_key_exists($key, $editions)) {
                $editions[$key]  = [];
            }
            $editions[$key][] = $edition;
        }

        $this->render('showcase/archives.html.twig', array('editions' => $editions));
    }

    public function couverture(Request $req, Response $rep) {
        $edition = $this->container->get_modelmanager('edition')->get($req->param('id'));
        if ($edition) {
            if ($edition['publique']) {
                $dir = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'];
                $file = $dir.'/couverture.png';
                if (is_file($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    header('Content-Length: '.filesize($file));
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    readfile($file);
                    throw HttpException::createFromCode(200);
                }
            }
        }
        throw HttpException::createFromCode(404);
    }

    public function edition_pdf(Request $req, Response $rep) {
        $edition = $this->container->get_modelmanager('edition')->get($req->param('id'));
        if ($edition) {
            if ($edition['publique']) {
                $dir = __DIR__ . '/../../private/editions/' . $edition['annee'] . '-' . $edition['mois'] . '-' . $edition['id'];
                $file = $dir . '/fti.pdf';
                if (is_file($file)) {
                    header('Content-Type: ' . mime_content_type($file));
                    header('Content-Length: ' . filesize($file));
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    readfile($file);
                    throw HttpException::createFromCode(200);
                }
            }
        }
        throw HttpException::createFromCode(404);
    }

}