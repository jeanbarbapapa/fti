<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 02/11/2018
 * Time: 18:43
 */

namespace FTI\Controler;


use BeardedByte\Application;
use BeardedByte\Controller;
use Klein\Exceptions\HttpException;
use Klein\Request;
use Klein\Response;

class Redaction extends Controller {

    /**
     * @var \FTI\Application
     */
    public $application;

    protected function _register_routes() {
        $this->router->respond(array('GET', 'POST'), '/connexion', [$this, 'connexion']);
        $this->router->respond('GET', '/deconnexion', [$this, 'deconnexion']);

        $this->router->with('/redaction', function() {
            $this->router->respond('*', function(Request $req, Response $rep) {
                if (!$this->application->isUserConnected) {
                    $rep->redirect('/connexion');
                    throw HttpException::createFromCode(200);
                }
            });

            $this->router->respond('GET', '/?', [$this, 'index']);
            $this->router->respond('GET', '/editions', [$this, 'editions']);
            $this->router->respond(array('GET', 'POST'), '/editions/nouvelle', [$this, 'nouvelle_edition']);
            $this->router->respond(array('GET', 'POST'), '/editions/[i:id]', [$this, 'voir_edition']);
            $this->router->respond(array('GET', 'POST'), '/editions/[i:id]/couverture', [$this, 'edition_couverture']);
            $this->router->respond(array('GET', 'POST'), '/editions/[i:id]/pdf', [$this, 'edition_pdf']);


            $this->router->respond('GET', '/dessins', [$this, 'dessins']);
            $this->router->respond(array('GET', 'POST'), '/dessins/nouveau', [$this, 'nouveau_dessin']);
            $this->router->respond(array('GET', 'POST'), '/dessins/[i:id]', [$this, 'voir_dessin']);
            $this->router->respond('GET', '/dessins/[i:id]/image', [$this, 'image_dessin']);

            $this->router->respond('GET', '/utilisateurs', [$this, 'utilisateurs']);
        });


    }

    public function connexion(Request $req, Response $rep) {

        if ($this->application->isUserConnected) {
            $rep->redirect('/');
            $rep->send();
            exit();
        }

        $email = "";
        $alert = "";

        if ($req->method('POST')) {
            $email = $req->paramsPost()->get('email');
            $passwordClearText = $req->paramsPost()->get('password');

            $users = $this->container->get_modelmanager('user')->select(['email' => $email]);

            if (count($users) == 1) {
                $user = $users[0];
                if ($passwordClearText ==  $user['password']) {
                    $_SESSION['user'] = $user;
                    $rep->redirect('/redaction');
                    $rep->send();
                    exit();
                }
            }

            $alert = "Oups... Les informations que vous avez indiquées sont erronées.";
        }

        $this->render('redaction/connexion.html.twig', array('email' => $email, 'alert' => $alert));
    }

    public function deconnexion(Request $req, Response $rep) {
        if ($this->application->isUserConnected) {
            // Unset session data
            $_SESSION = array();

            // Destroy the session related cookies
            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                );
            }

            session_destroy();
        }

        $rep->redirect('/');
    }

    public function index(Request $req, Response $rep) {
        $this->render('/redaction/index.html.twig');
    }

    public function editions(Request $req, Response $rep) {
        $editions = $this->container->get_modelmanager('edition')->get_all();
        $this->render('/redaction/edition/liste.html.twig', array('editions' => $editions));
    }

    public function voir_edition(Request $req, Response $rep) {
        $edition = $this->container->get_modelmanager('edition')->get($req->param('id'));
        if ($edition) {

            if ($req->method('POST')) {
                $em = $this->container->get_modelmanager('edition');

                $oldDir = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'];
                $edition = $em->update_from($edition, $_POST, ['annee', 'mois', 'titre', 'description', 'publique']);
                if (!isset($_POST['publique'])) {
                    $edition['publique'] = 0;
                }
                $newDir = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'];
                $em->update($edition);

                if (is_dir($oldDir)) {
                    exec("mv \"$oldDir\" \"$newDir\"");
                }


                $rep->redirect('/redaction/editions/'.$edition['id']);
                throw HttpException::createFromCode(200);
            }

            $couvertureFile = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'].'/couverture.png';
            $pdfFile = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'].'/fti.pdf';

            if (is_file($couvertureFile)) {
                $edition['couverture'] = 1;
            }
            if (is_file($pdfFile)) {
                $edition['pdf'] = 1;
            }

            $alerts = [];



            $this->render('/redaction/edition/voir.html.twig', array('edition' => $edition));
            throw HttpException::createFromCode(200);
        }
        throw HttpException::createFromCode(404);
    }

    public function nouvelle_edition(Request $req, Response $rep) {
        $edition = [];
        $alerts = [];

        if ($req->method('POST')) {
            $em = $this->container->get_modelmanager('edition');
            $edition = $em->build_from($_POST, ['annee', 'mois', 'titre', 'description', 'publique']);
            $em->insert($edition, ['date_ajout' => "datetime('now')"]);
            mkdir(__DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id']);
            $rep->redirect('/redaction/editions/'.$edition['id']);
            throw HttpException::createFromCode(200);
        }

        $this->render('/redaction/edition/nouvelle.html.twig', array('edition' => $edition, 'alerts' => $alerts));
    }

    public function edition_couverture(Request $req, Response $rep) {
        $edition = $this->container->get_modelmanager('edition')->get($req->param('id'));
        if ($edition) {

            if ($req->method('POST')) {

                if (isset($_FILES['couverture']) && $_FILES['couverture']['error'] != 4) {

                    $uploadDir = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'];

                    $parts = explode('.', $_FILES['couverture']['name']);
                    $fileExtension = array_pop($parts);


                    if (strtolower($fileExtension) == "png") {
                        $filepath = $uploadDir.'/couverture.png';
                        if (is_file($filepath)) {
                            unlink($filepath);
                        }
                        move_uploaded_file($_FILES['couverture']['tmp_name'], $filepath);
                    }
                }

                $rep->redirect('/redaction/editions/'.$edition['id']);
                throw HttpException::createFromCode(200);
            } else {
                $dir = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'];
                $file = $dir.'/couverture.png';
                if (is_file($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    header('Content-Length: '.filesize($file));
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    readfile($file);
                    throw HttpException::createFromCode(200);
                }
            }
        }
        throw HttpException::createFromCode(404);
    }

    public function edition_pdf(Request $req, Response $rep) {
        $edition = $this->container->get_modelmanager('edition')->get($req->param('id'));
        if ($edition) {
            if ($req->method('POST')) {

                if (isset($_FILES['pdf']) && $_FILES['pdf']['error'] != 4) {

                    $uploadDir = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'];

                    $parts = explode('.', $_FILES['pdf']['name']);
                    $fileExtension = array_pop($parts);


                    if (strtolower($fileExtension) == "pdf") {
                        $filepath = $uploadDir.'/fti.pdf';
                        if (is_file($filepath)) {
                            unlink($filepath);
                        }
                        move_uploaded_file($_FILES['pdf']['tmp_name'], $filepath);
                    }
                }

                $rep->redirect('/redaction/editions/'.$edition['id']);
                throw HttpException::createFromCode(200);
            } else {
                $dir = __DIR__.'/../../private/editions/'.$edition['annee'].'-'.$edition['mois'].'-'.$edition['id'];
                $file = $dir.'/fti.pdf';
                if (is_file($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    header('Content-Length: '.filesize($file));
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    readfile($file);
                    throw HttpException::createFromCode(200);
                }
            }
        }
        throw HttpException::createFromCode(404);
    }

    public function dessins(Request $req, Response $rep) {

        $dessins = $this->container->get_modelmanager('dessin')->get_all();
        $this->render('redaction/dessin/index.html.twig', array('dessins' => $dessins));
    }

    public function nouveau_dessin(Request $req, Response $rep) {

        $error = false;
        $rm = $this->container->get_modelmanager('dessin');
        $dessin = $rm->create();

        if ($req->method('POST')) {

            $dessin = $rm->build_from($_POST, ['nom', 'description', 'artiste', 'publique']);
            // Gestion des checkbox
            if (!isset($_POST['publique'])) {
                $dessin['publique'] = 0;
            }

            if (isset($_FILES['file']) && $_FILES['file']['error'] != 4) {

                $uploadDir = __DIR__.'/../../private/dessins';

                $parts = explode('.', $_FILES['file']['name']);
                $fileExtension = array_pop($parts);
                $filename = join('.', $parts);
                $acc = 0;
                $filepath = $uploadDir.'/'.$filename.'.'.$fileExtension;

                while (is_file($filepath)) {
                    $acc += 1;
                    $filepath = $uploadDir.'/'.$filename.strval($acc).'.'.$fileExtension;
                }

                var_dump($filepath);

                if (move_uploaded_file($_FILES['file']['tmp_name'], $filepath)) {
                    $dessin['filepath'] = $filepath;
                    $rm->insert($dessin, ['date_publication' => 'DATETIME("now")']);
                    $rep->redirect('/redaction/dessins/'.$dessin['id']);
                    throw HttpException::createFromCode(200);
                } else {
                    $error = true;
                }
            }
        }

        $this->render('redaction/dessin/nouveau.html.twig', array('error' => $error, 'dessin' => $dessin));
    }

    public function voir_dessin(Request $req, Response $rep) {
        $rm = $this->container->get_modelmanager('dessin');
        $dessin = $rm->get($req->param('id'));
        if ($dessin) {
            if ($req->method('POST')) {
                $dessin = $rm->update_from($dessin, $_POST, ['nom', 'description', 'artiste', 'publique']);
                if (!isset($_POST['publique'])) {
                    $dessin['publique'] = 0;
                }

                if (isset($_FILES['file']) && $_FILES['file']['error'] != 4) {

                    $uploadDir = __DIR__.'/../../private/dessins';

                    $parts = explode('.', $_FILES['file']['name']);
                    $fileExtension = array_pop($parts);
                    $filename = join('.', $parts);
                    $acc = 0;
                    $filepath = $uploadDir.'/'.$filename.'.'.$fileExtension;

                    while (is_file($filepath)) {
                        $acc += 1;
                        $filepath = $uploadDir.'/'.$filename.strval($acc).'.'.$fileExtension;
                    }

                    if (move_uploaded_file($_FILES['file']['tmp_name'], $filepath)) {
                        $dessin['filepath'] = $filepath;
                    }
                }

                $rm->update($dessin);
            }
            $dessin['file_available'] = is_file($dessin['filepath']);
            $this->render('redaction/dessin/info.html.twig', array('dessin' => $dessin));
            throw HttpException::createFromCode(200);
        }

        throw HttpException::createFromCode(404);
    }

    public function image_dessin(Request $req, Response $rep) {
        $rm = $this->container->get_modelmanager('dessin');
        $dessin = $rm->get($req->param('id'));
        if ($dessin) {
            if (is_file($dessin['filepath'])) {
                header('Content-Type: '.mime_content_type($dessin['filepath']));
                header('Content-Length: '.filesize($dessin['filepath']));
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                readfile($dessin['filepath']);
                throw HttpException::createFromCode(200);
            }
        }

        throw HttpException::createFromCode(404);
    }

    public function utilisateurs(Request $req, Response $rep) {

        if (!$this->application->isUserAdmin) {
            $rep->redirect('/redaction');
            throw HttpException::createFromCode(200);
        }

        $users = $this->container->get_modelmanager('user')->get_by_promo();

        $this->render('redaction/utilisateurs.html.twig', array('users' => $users));
    }

}