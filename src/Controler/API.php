<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 02/11/2018
 * Time: 18:43
 */

namespace FTI\Controler;


use BeardedByte\Application;
use Klein\Exceptions\HttpException;
use Klein\Request;
use Klein\Response;

class API extends \BeardedByte\API {

    /**
     * @var \FTI\Application
     */
    public $application;

    protected function _register_routes() {
        $this->router->with('/api', function() {
            /* REDACTION */
            $this->router->with('/redaction', function() {

                /* FIREWALL */
                $this->router->respond('*', function (Request $req, Response $rep) {
                    if (!$this->application->isUserConnected) {
                        throw HttpException::createFromCode(401);
                    }
                });

                /* UTILISATEURS */
                $this->router->with('/utilisateurs', function () {

                    /* FIREWALL */
                    $this->router->respond('*', function (Request $req, Response $rep) {
                        if (!$this->application->isUserAdmin) {
                            throw HttpException::createFromCode(403);
                        }
                    });
                    $this->router->respond('GET', '/?', function (Request $req, Response $rep) {
                        $userManager = $this->container->get_modelmanager('user');
                        $users = $userManager->get_all();
                        $rep->json($this->successStatement($users));
                    });
                    $this->router->respond('GET', '/[i:id]', function (Request $req, Response $rep) {
                        $userManager = $this->container->get_modelmanager('user');
                        $user = $userManager->get($req->param('id'));
                        if ($user) {
                            $rep->json($this->successStatement($user));
                        } else {
                            $rep->json($this->notFoundStatement());
                        }
                    });
                    $this->router->respond('POST', '/?', function (Request $req, Response $rep) {
                        $userManager = $this->container->get_modelmanager('user');
                        $user = $userManager->build_from($_POST, ['prenom', 'nom', 'fonction', 'email', 'password', 'promo', 'admin']);
                        $userManager->insert($user);
                        $rep->json($this->successStatement(null));
                    });
                    $this->router->respond('POST', '/[i:id]', function (Request $req, Response $rep) {
                        $userManager = $this->container->get_modelmanager('user');
                        $_user = $userManager->get($req->param('id'));
                        if ($_user) {
                            $user = $userManager->update_from($_user, $_POST, ['prenom', 'nom', 'fonction', 'email', 'password', 'promo', 'admin']);
                            $userManager->update($user);
                            $rep->json($this->successStatement(null));
                        } else {
                            $rep->json($this->notFoundStatement());
                        }
                    });
                    $this->router->respond('DELETE', '/[i:id]', function (Request $req, Response $rep) {
                        $userManager = $this->container->get_modelmanager('user');
                        $_user = $userManager->get($req->param('id'));
                        if ($_user) {
                            $userManager->delete($_user);
                            $rep->json($this->successStatement(null));
                        } else {
                            $rep->json($this->notFoundStatement());
                        }
                    });
                });
            });
        });
    }

}