<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 02/11/2018
 * Time: 18:39
 */

namespace FTI\Model;


use BeardedByte\ModelManager;

class User extends ModelManager {
    public function __construct(\PDO $database) {
        parent::__construct($database, 'user', true);
    }

    public function get_by_promo() {
        $sql = "SELECT * FROM user ORDER BY promo DESC";
        $req = $this->database->prepare($sql);
        $req->execute(array());
        return $req->fetchAll(\PDO::FETCH_ASSOC);
    }
}